package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import impl.Iterator;
import interfaces.NodeType;

class IteratorTest {

	impl.BinaryTree<Integer> binaryTree = new impl.BinaryTree<Integer>();

	@Test
	void getValueShouldReturnTheValueOfTheCurrentNode() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.setValue(1);

		it.goRoot();

		assertEquals(1, it.getValue());
	}

	@Test
	void goLeftShouldMakeIteratorGoLeft() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goLeft();
		it.addValue(2);
		it.goRoot();

		// Test
		assertEquals(1, it.getValue());

		it.goLeft();

		assertEquals(2, it.getValue());
	}
	
	@Test
	void goLeftShouldThrowAnErrorIfNodeIsSentinel() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goLeft();
		it.addValue(2);
		it.goLeft();

		// test
		Exception thrown = Assertions.assertThrows(Exception.class, () -> {
			it.goLeft();
		});

		Assertions.assertEquals("We are in a sentinel node, so we cannot go left", thrown.getMessage());
	}

	@Test
	void goRightShouldMakeIteratorGoRight() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRoot();

		// Test
		assertEquals(1, it.getValue());

		it.goRight();

		assertEquals(2, it.getValue());
	}
	
	@Test
	void goRightShouldThrowAnErrorIfNodeIsSentinel() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();

		// test
		Exception thrown = Assertions.assertThrows(Exception.class, () -> {
			it.goRight();
		});

		Assertions.assertEquals("We are in a sentinel node, so we cannot go right", thrown.getMessage());
	}
	
	@Test
	void goUpShouldMakeIteratorGoUp() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();
		it.addValue(3);

		// Test
		assertEquals(3, it.getValue());

		it.goUp();
		assertEquals(2, it.getValue());

		it.goUp();
		assertEquals(1, it.getValue());
	}
	
	@Test
	void goUpShouldThrowIfIteratorIsOnTheRootNode() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.goRoot();

		Exception thrown = Assertions.assertThrows(Exception.class, () -> {
			// test
			it.goUp();
		});

		Assertions.assertEquals("We are in the root node, we cannot go up", thrown.getMessage());
	}
	
	@Test
	void addValueNotNullShouldGiveTheCurrentNodeAsTheParentOfTheNewChildren() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();
		it.addValue(3);
		
		//Test
		assertEquals(NodeType.LEAF, it.nodeType());

		it.goRight();
		assertEquals(NodeType.SENTINEL, it.nodeType());

		it.goUp();
		assertEquals(3, it.getValue());
		
		it.goLeft();
		assertEquals(NodeType.SENTINEL, it.nodeType());
		
		it.goUp();
		assertEquals(3, it.getValue());	
	}

	@Test
	void addValueOnExistingNodeShouldThrow() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.addValue(1);
		it.goRight();
		it.addValue(2);

		Exception thrown = Assertions.assertThrows(Exception.class, () -> {
			// test
			it.addValue(2);
		});

		Assertions.assertEquals("We are not in a sentinel node, we cannot use addValue", thrown.getMessage());
	}
	
	@Test
	void getValueOnSentinelNodeShouldThrow() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.setValue(1);
		it.goRight();

		Exception thrown = Assertions.assertThrows(Exception.class, () -> {
			// test
			it.getValue();
		});

		Assertions.assertEquals("We are in a sentinel node, we cannot use getValue", thrown.getMessage());
	}
	
	@Test
	void setValueOnSentinelShouldThrow() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.setValue(1);
		it.goRight();

		Exception thrown = Assertions.assertThrows(Exception.class, () -> {
			// test
			it.setValue(2);
		});

		Assertions.assertEquals("We are in a sentinel node, we cannot use setValue", thrown.getMessage());
	}
	
	@Test
	void isEmptyShouldReturnTrueIfTheNodeIsSentinel() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.setValue(1);

		it.goRight();

		assertEquals(true, it.isEmpty());
	}
	
	@Test
	void isEmptyShouldReturnFalseIfTheNodeIsNotSentinel() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.setValue(1);
		it.goRight();
		it.addValue(2);

		assertEquals(false, it.isEmpty());
	}
	
	@Test
	void nodeTypeShouldReturnSentinel() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.setValue(1);
		it.goRight();
		
		assertEquals(NodeType.SENTINEL, it.nodeType());
	}
	
	@Test
	void nodeTypeShouldReturnSimpleLeft() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.setValue(1);
		it.goLeft();
		it.addValue(2);
		it.goRoot();
		
		assertEquals(NodeType.SIMPLE_LEFT, it.nodeType());
	}
	
	@Test
	void nodeTypeShouldReturnSimpleRight() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRoot();
		
		assertEquals(NodeType.SIMPLE_RIGHT, it.nodeType());
	}
	
	@Test
	void nodeTypeShouldReturnDouble() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRoot();
		it.goLeft();
		it.addValue(3);
		it.goRoot();
		
		assertEquals(NodeType.DOUBLE, it.nodeType());
	}
	
	@Test
	void nodeTypeShouldReturnLeaf() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		
		assertEquals(NodeType.LEAF, it.nodeType());
	}
	
	@Test
	void clearShouldClearTheSubTree() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();
		it.addValue(3);
		it.goUp();
		it.goLeft();
		it.addValue(4);
		it.goRight();
		it.addValue(5);
		it.goRoot();
		it.goRight();
		
		// Test
		assertEquals(2, it.getValue());
		assertEquals(NodeType.DOUBLE, it.nodeType());
		
		it.clear();
		assertEquals(NodeType.SENTINEL, it.nodeType());
		
		it.goUp();
		assertEquals(1, it.getValue());
	}
	
	@Test
	void shouldClearRootNode() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goUp();
		it.goLeft();
		it.addValue(3);
		it.goRoot();
		
		// Test
		assertEquals(1, it.getValue());
		assertEquals(NodeType.DOUBLE, it.nodeType());
		
		it.clear();
		assertEquals(NodeType.SENTINEL, it.nodeType());
	}
	
	@Test
	void removedNodeIsOnTheLeftOfItsParentAndIsSimpleRight() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();
		it.addValue(3);
		it.goUp();
		it.goLeft();
		it.addValue(4);
		it.goRight();
		it.addValue(5);
		it.goLeft();
		it.addValue(6);

		it.goRoot();
		it.goRight();
		it.goLeft();
		
		// Test
		assertEquals(4, it.getValue());
		
		it.remove();
		assertEquals(5, it.getValue());
		
		it.goLeft();
		assertEquals(6, it.getValue());
	}
	
	@Test
	void removedNodeIsOnTheRightSideOfItsParentAndIsSimpleRight() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goLeft();
		it.addValue(3);
		it.goUp();
		it.goRight();
		it.addValue(4);
		it.goRight();
		it.addValue(5);
		it.goLeft();
		it.addValue(6);

		it.goRoot();
		it.goRight();
		it.goRight();
		
		// Test
		assertEquals(4, it.getValue());
		
		it.remove();
		assertEquals(5, it.getValue());
		
		it.goLeft();
		assertEquals(6, it.getValue());
	}
	
	@Test
	void removedNodeIsOnTheRightSideOfItsParentAndIsSimpleLeft() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goLeft();
		it.addValue(3);
		it.goUp();
		it.goRight();
		it.addValue(4);
		it.goLeft();
		it.addValue(5);
		it.goLeft();
		it.addValue(6);

		it.goRoot();
		it.goRight();
		it.goRight();
		
		// Test
		assertEquals(4, it.getValue());
		
		it.remove();
		assertEquals(5, it.getValue());
		
		it.goLeft();
		assertEquals(6, it.getValue());
	}
	
	@Test
	void removedNodeIsOnTheLeftOfItsParentAndIsSimpleLeft() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();
		it.addValue(3);
		it.goUp();
		it.goLeft();
		it.addValue(4);
		it.goLeft();
		it.addValue(5);
		it.goLeft();
		it.addValue(6);

		it.goRoot();
		it.goRight();
		it.goLeft();
		
		// Test
		assertEquals(4, it.getValue());
		
		it.remove();
		assertEquals(5, it.getValue());
		
		it.goLeft();
		assertEquals(6, it.getValue());
	}
	
	@Test
	void removedNodeIsALeaf() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();
		it.addValue(3);
		it.goUp();
		it.goLeft();
		it.addValue(4);

		// Test
		assertEquals(NodeType.LEAF, it.nodeType());
		it.remove();

		assertEquals(NodeType.SENTINEL, it.nodeType());
		
		it.goUp();
		assertEquals(2, it.getValue());
	}
	
	@Test
	void removedNodeIsASentinelShouldHaveNoImpact() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();
		it.addValue(3);
		it.goUp();
		it.goLeft();
		it.addValue(4);
		it.goRight();

		// Test
		assertEquals(NodeType.SENTINEL, it.nodeType());
		it.remove();

		assertEquals(NodeType.SENTINEL, it.nodeType());
		
		it.goUp();
		assertEquals(4, it.getValue());
	}
	
	@Test
	void removeShouldThrowWhenRemovedNodeIsDouble() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();
		it.addValue(3);
		it.goUp();
		it.goLeft();
		it.addValue(4);
		it.goUp();

		Exception thrown = Assertions.assertThrows(Exception.class, () -> {
			// test
			it.remove();
		});

		Assertions.assertEquals("It is a double node, we cannot remove it", thrown.getMessage());
	}
	
	@Test
	void switchNodeShouldThrowIfNumberLessThan0() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();
		it.addValue(3);
		it.goUp();
		it.goLeft();
		it.addValue(4);
		it.goRight();
		it.addValue(5);
		it.goLeft();
		it.addValue(6);

		it.goUp();
		
		Exception thrown = Assertions.assertThrows(Exception.class, () -> {
			// test
			it.switchValue(-1);
		});

		Assertions.assertEquals("parameter is < 0", thrown.getMessage());
	}
	
	@Test
	void switchNodeShouldThrowIfNumberIsTooHigh() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();
		it.addValue(3);
		it.goUp();
		it.goLeft();
		it.addValue(4);
		it.goRight();
		it.addValue(5);
		it.goLeft();
		it.addValue(6);
		
		Exception thrown = Assertions.assertThrows(Exception.class, () -> {
			// test
			it.switchValue(5);
		});

		Assertions.assertEquals("parameter is too high (cannot go further than root)", thrown.getMessage());
	}
	
	@Test
	void switchNodeShouldSwitchCorrectlyBetweenRootAndSentinel() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();
		it.addValue(3);
		it.goUp();
		it.goLeft();
		it.addValue(4);
		it.goRight();
		it.addValue(5);
		it.goLeft();
		it.addValue(6);
		
		//Test
		it.switchValue(4);
		
		assertEquals(1, it.getValue());
		
		it.goUp();
		assertEquals(5, it.getValue());
		
		it.goUp();
		assertEquals(4, it.getValue());
		
		it.goUp();
		assertEquals(2, it.getValue());
		
		it.goUp();
		assertEquals(6, it.getValue());
	}
	
	@Test
	void switchNodeShouldSwitchCorrectlyBetweenRandomNodes() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();

		// Setup
		it.setValue(1);
		it.goRight();
		it.addValue(2);
		it.goRight();
		it.addValue(3);
		it.goUp();
		it.goLeft();
		it.addValue(4);
		it.goRight();
		it.addValue(5);
		it.goLeft();
		it.addValue(6);
		
		//Test
		it.goUp();
		it.switchValue(2);
		
		assertEquals(2, it.getValue());
		
		it.goUp();
		assertEquals(4, it.getValue());
		
		it.goUp();
		assertEquals(5, it.getValue());
		
		it.goUp();
		assertEquals(1, it.getValue());
	}
}
