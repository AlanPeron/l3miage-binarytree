package test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import impl.Iterator;

class BinaryTreeTest {
	
	impl.BinaryTree<Integer> binaryTree = new impl.BinaryTree<Integer>();

	@Test
	void emptyBinaryTreeShouldBeEmpty() {
		assertEquals(binaryTree.isEmpty(), true);
	}
	
	@Test
	void notEmptyBinaryTreeShouldNotBeEmpty() throws Exception {
		Iterator<Integer> it = binaryTree.iterator();
		it.setValue(1);

		assertEquals(binaryTree.isEmpty(), false);
	}

}
