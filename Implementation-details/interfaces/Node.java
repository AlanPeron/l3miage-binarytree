package interfaces;

import interfaces.TreePrinter.PrintableNode;

public class Node<T> implements PrintableNode {
	T value;
	Node<T> parent;
	Node<T> leftChild;
	Node<T> rightChild;

	public Node(T value) {
		this.value = value;

		if(value != null) {
			this.setLeftChild(new Node<T>(null));
			this.setRightChild(new Node<T>(null));
		}
	}
	
	public void setValue(T value) {
		this.value = value;
	}
	
	public T getValue() {
		return this.value;
	}
	
	public Node<T> getLeftChild() {
		return this.leftChild;
	}
	
	public void setLeftChild(Node<T> leftChild) {
		this.leftChild = leftChild;
		leftChild.setParent(this);
	}
	
	public Node<T> getRightChild() {
		return this.rightChild;
	}
	
	public void setRightChild(Node<T> rightChild) {
		this.rightChild = rightChild;
		rightChild.setParent(this);
	}
	
	public Node<T> getParent() {
		return this.parent;
	}
	
	public void setParent(Node<T> parent) {
		this.parent = parent;
	}
	
	public String toString() {
		if (this.value == null) {
			return "";
		}

		return this.value.toString();
	}
}
