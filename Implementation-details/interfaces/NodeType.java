package interfaces;

public enum NodeType {
	SENTINEL,
	LEAF,
	SIMPLE_LEFT,
	SIMPLE_RIGHT,
	DOUBLE
}
