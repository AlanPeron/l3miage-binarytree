package interfaces;

public interface IBinaryTree<T> {
	public IIterator<T> iterator();
	
	public boolean isEmpty();
}
