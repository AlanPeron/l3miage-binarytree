package interfaces;
public interface IIterator<T> {
	public void goLeft() throws Exception;
	
	public void goRight() throws Exception;
	
	public void goUp() throws Exception;
	
	public void goRoot() throws Exception;
	
	public boolean isEmpty();
	
	NodeType nodeType() throws Exception;
	
	public void remove() throws Exception;
	
	public void clear();
	
	T getValue() throws Exception;
	
	void addValue(T v) throws Exception;
	
	void setValue(T v) throws Exception;
	
	void switchValue(int i) throws Exception;
}
