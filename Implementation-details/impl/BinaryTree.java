package impl;

import interfaces.IBinaryTree;
import interfaces.IIterator;
import interfaces.Node;
import interfaces.TreePrinter;

public class BinaryTree<T> implements IBinaryTree<T> {
	protected Node<T> rootNode;
	
	public BinaryTree() {
		this.rootNode = new Node<T>(null);
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>(this);
	}

	@Override
	public boolean isEmpty() {
		return rootNode.getValue() == null;
	}

	public void print() {
		TreePrinter.print(rootNode);
	}
}
