package impl;

import interfaces.IIterator;
import interfaces.MovementEnum;
import interfaces.Node;
import interfaces.NodeType;

public class Iterator<T> implements IIterator<T> {
	private BinaryTree<T> binaryTree;
	private Node<T> parentNode;
	private Node<T> currentNode;
	private MovementEnum lastMovement;

	public Iterator(BinaryTree<T> binaryTree) {
		this.binaryTree = binaryTree;
		this.currentNode = binaryTree.rootNode;
		
		if (this.currentNode.getRightChild() == null) {
			this.currentNode.setRightChild(new Node<T>(null));
		}
		if (this.currentNode.getLeftChild() == null) {
			this.currentNode.setLeftChild(new Node<T>(null));
		}
	}

	@Override
	public void goLeft() throws Exception {
		if (this.nodeType() == NodeType.SENTINEL) {
			throw new Exception("We are in a sentinel node, so we cannot go left");
		} else {
			this.moveToNode(this.currentNode.getLeftChild());
			lastMovement = MovementEnum.LEFT;
		}
	}

	@Override
	public void goRight() throws Exception {
		if (this.nodeType() == NodeType.SENTINEL) {
			throw new Exception("We are in a sentinel node, so we cannot go right");
		} else {
			this.moveToNode(this.currentNode.getRightChild());
			lastMovement = MovementEnum.RIGHT;	
		}
	}

	@Override
	public void goUp() throws Exception {
		if (this.binaryTree.rootNode == this.currentNode) {
			throw new Exception("We are in the root node, we cannot go up");
		}

		this.moveToNode(this.currentNode.getParent());
	}

	@Override
	public void goRoot() throws Exception {
		this.moveToNode(binaryTree.rootNode);
	}

	@Override
	public boolean isEmpty() {
		return this.currentNode.getValue() == null;
	}

	@Override
	public NodeType nodeType() throws Exception {
		if (this.currentNode.getValue() == null)
			return NodeType.SENTINEL;

		if (this.currentNode.getLeftChild().getValue() != null && this.currentNode.getRightChild().getValue() != null) {
			return NodeType.DOUBLE;
		}

		if (this.currentNode.getLeftChild().getValue() != null) {
			return NodeType.SIMPLE_LEFT;
		}

		if (this.currentNode.getRightChild().getValue() != null) {
			return NodeType.SIMPLE_RIGHT;
		}

		if (this.currentNode.getLeftChild().getValue() == null && this.currentNode.getRightChild().getValue() == null) {
			return NodeType.LEAF;
		}

		throw new Exception("You don't seem to be in a valid state");
	}

	@Override
	public void remove() throws Exception {
		if (this.nodeType() != NodeType.SENTINEL) {
			Node<T> toRemove = this.currentNode;
			Node<T> greatChild = new Node<T>(null);
			Node<T> grandParent = toRemove.getParent();

			if (this.nodeType() == NodeType.SIMPLE_LEFT) {
				greatChild = toRemove.getLeftChild();
			} else if (this.nodeType() == NodeType.SIMPLE_RIGHT) {
				greatChild = toRemove.getRightChild();
			} else if (this.nodeType() == NodeType.DOUBLE) {
				throw new Exception("It is a double node, we cannot remove it");
			}

			if (grandParent.getLeftChild() == toRemove) {
				grandParent.setLeftChild(greatChild);
			} else {
				grandParent.setRightChild(greatChild);
			}

			this.moveToNode(greatChild);
		}
	}

	@Override
	public void clear() {
		Node<T> parent = this.currentNode.getParent();
		this.currentNode = new Node<T>(null);
		this.currentNode.setParent(parent);
		this.parentNode = this.currentNode.getParent();
	}

	@Override
	public T getValue() throws Exception {
		if (this.nodeType() == NodeType.SENTINEL) {
			throw new Exception("We are in a sentinel node, we cannot use getValue");
		}

		return this.currentNode.getValue();
	}

	@Override
	public void addValue(T v) throws Exception {
		if (this.nodeType() == NodeType.SENTINEL) {
			Node<T> newNode = new Node<T>(v);

			if (this.lastMovement == MovementEnum.LEFT) {
				this.parentNode.setLeftChild(newNode);
			} else if (this.lastMovement == MovementEnum.RIGHT) {
				this.parentNode.setRightChild(newNode);
			}

			this.currentNode = newNode;
		} else {
			throw new Exception("We are not in a sentinel node, we cannot use addValue");
		}
	}

	@Override
	public void setValue(T v) throws Exception {
		if (this.nodeType() != NodeType.SENTINEL || this.binaryTree.rootNode == this.currentNode) {
			this.currentNode.setValue(v);
		} else {
			throw new Exception("We are in a sentinel node, we cannot use setValue");
		}
	}
	
	private void moveToNode(Node<T> moveTo) throws Exception {
		this.currentNode = moveTo;
		this.parentNode = moveTo.getParent();
		
		if (this.parentNode == null && this.currentNode != this.binaryTree.rootNode) {
			throw new Exception("Something went wrong, the node we are trying to reach has no parent and is not the root node");
		}
		if (this.currentNode != this.binaryTree.rootNode && this.parentNode.getLeftChild() != this.currentNode && this.parentNode.getRightChild() != this.currentNode) {
			throw new Exception("Something went wrong, The parent of the node none of its child equal to the current node");
		}
	}

	@Override
	public void switchValue(int i) throws Exception {
		if ( i < 0) throw new Exception("parameter is < 0");
		
		Node<T> ascendantNode = this.currentNode;
		
		while (i > 0) {
			if (ascendantNode.getParent() == null) {
				throw new Exception("parameter is too high (cannot go further than root)");
			} else {
				ascendantNode = ascendantNode.getParent();
			}

			i--;
		}
		
		T ascendantNodeValue = ascendantNode.getValue();
		ascendantNode.setValue(this.currentNode.getValue());
		this.currentNode.setValue(ascendantNodeValue);
	}

}
