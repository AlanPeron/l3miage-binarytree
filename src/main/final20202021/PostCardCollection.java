package main.final20202021;

import impl.BinaryTree;
import impl.Iterator;
import interfaces.Node;

public class PostCardCollection extends BinaryTree<Postcard> {
	public PostCardCollection() throws Exception {
		super();
		setUp();
	}

	public void setUp() throws Exception {
		this.rootNode = new Node<Postcard>(new Postcard(21, "ed1", "le1", 2, 0));
		
		Iterator<Postcard> iterator = this.iterator();
		iterator.goLeft();
		iterator.addValue(new Postcard(8, "ed2", "le2", 1, 0));
		iterator.goLeft();
		iterator.addValue(new Postcard(1, "ed4", "le4", -1, 0));
		iterator.goRight();
		iterator.addValue(new Postcard(5, "ed8", "le8", -1, 0));
		iterator.goUp();
		iterator.goUp();
		iterator.goRight();
		iterator.addValue(new Postcard(9, "ed5", "le5", -1, 0));
		
		iterator.goRoot();

		iterator.goRight();
		iterator.addValue(new Postcard(32, "ed3", "le3", 0, 0));
		iterator.goLeft();
		iterator.addValue(new Postcard(25, "ed6", "le6", -1, 0));
		iterator.goUp();
		iterator.goRight();
		iterator.addValue(new Postcard(52, "ed7", "le7", 1, 0));
		iterator.goLeft();
		iterator.addValue(new Postcard(50, "ed9", "le9", 0, 0));
		iterator.goLeft();
		iterator.addValue(new Postcard(43, "ed10", "le10", -1, 0));
	}

	/**
	 * To Implement
	 */
	public boolean findPostCard(int postcardID) {
		return false;
	}

	/**
	 * To Implement
	 */
	public void addPostOrderNumbers() {
		
	}

	/**
	 * To Implement
	 */
	public int height() {
		return 0;
	}

	/**
	 * To Implement
	 */
	public String longestLegend() {
		return "";
	}

	/**
	 * To Implement
	 */
	public boolean isNodeBalancedTree() {
		return false;
	}
}
