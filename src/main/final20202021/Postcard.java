package main.final20202021;

public class Postcard {
	public int postcardID;
	public String editor;
	public String legend;
	public int leftHeight;
	public int extraField;
	
	public Postcard(int postcardID, String editor, String legend, int leftHeight, int extraField) {
		this.editor = editor;
		this.legend = legend;
		this.postcardID = postcardID;
		this.extraField = extraField;
		this.leftHeight = leftHeight;
	}

	public boolean equals(Postcard car) {
		return car.postcardID == this.postcardID;
	}
	
	public String toString() {
		return "(postcardID: " + postcardID + ", editor: " + editor + ", legend: " + legend + ", leftHeight: " + leftHeight + ", extraField: " + extraField + ")";
	}
}
