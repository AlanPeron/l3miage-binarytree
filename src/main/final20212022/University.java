package main.final20212022;

public class University implements Comparable<University> {
	public String name;
	public String country;
	public int studentNumber;
	public int height;
	
	public University(String name, String country, int studentNumber, int height) {
		this.name = name;
		this.country = country;
		this.studentNumber = studentNumber;
		this.height = height;
	}
	
	@Override
	public int compareTo(University university) {
		return this.name.compareTo(university.name);
	}
	
	public String toString() {
		return "(" + name + ", " + country + ", " + studentNumber + ", " + height + ")";
	}
}
