package main.final20212022;

public class Main {
	public static void main(String[] args) throws Exception {
		UniversityIndex universityIndex = new UniversityIndex();

		//Make your function calls here. Examples:
		System.out.println(universityIndex.isPresent(new University("", "", 0, 0))); 
		System.out.println(universityIndex.count(1000));
		System.out.println(universityIndex.diameter());
		universityIndex.printSomeNodes(2);

		universityIndex.print();
	}
}
