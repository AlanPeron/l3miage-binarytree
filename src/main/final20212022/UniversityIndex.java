package main.final20212022;

import impl.BinaryTree;
import impl.Iterator;
import interfaces.Node;

public class UniversityIndex extends BinaryTree<University> {
	public UniversityIndex() throws Exception {
		super();
		setUp();
	}

	public void setUp() throws Exception {
		this.rootNode = new Node<University>(new University("u2", "p1", 2000, 4));
		
		Iterator<University> iterator = this.iterator();
		iterator.goLeft();
		iterator.addValue(new University("u1", "p1", 1000, 0));
		iterator.goUp();
		iterator.goRight();
		iterator.addValue(new University("u6", "p1", 4000, 3));
		iterator.goLeft();
		iterator.addValue(new University("u5", "p3", 1000, 2));
		iterator.goLeft();
		iterator.addValue(new University("u3", "p2", 3000, 1));
		iterator.goRight();
		iterator.addValue(new University("u4", "p3", 2000, 0));
		iterator.goUp();
		iterator.goUp();
		iterator.goUp();
		iterator.goRight();
		iterator.addValue(new University("u99", "p1", 2000, 2));
		iterator.goLeft();
		iterator.addValue(new University("u8", "p3", 3000, 1));
		iterator.goLeft();
		iterator.addValue(new University("u7", "p2", 2000, 0));
		iterator.goUp();
		iterator.goRight();
		iterator.addValue(new University("u9", "p4", 1000, 0));
	}

	/**
	 * To Implement
	 * @throws Exception 
	 */
	public boolean isPresent(University university) throws Exception {
		return false;
	}

	/**
	 * To Implement
	 */
	public void printSomeNodes(int depth) {

	}

	/**
	 * To Implement
	 */
	public int count(int size) {
		return 0;
	}

	/**
	 * To Implement
	 */
	public int diameter() {
		return 0;
	}
}
