package main.final20222023;

public class Main {
	public static void main(String[] args) throws Exception {
		Garage garage = new Garage();

		//Make your function calls here
		System.out.println(garage.getQuantity(new Car("", "", 0, 0))); 
		System.out.println(garage.countCars(""));
		System.out.println(garage.total());
		System.out.println(garage.rightmostLeaf());
		garage.printClosestLeaf();

		garage.print();
	}
}
