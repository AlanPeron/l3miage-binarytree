package main.final20222023;

public class Car implements Comparable<Car> {
	public String constructor;
	public String model;
	public int quantity;
	public int leftTotal;
	
	public Car(String constructor, String model, int quantity, int leftTotal) {
		this.constructor = constructor;
		this.model = model;
		this.quantity = quantity;
		this.leftTotal = leftTotal;
	}
	
	@Override
	public int compareTo(Car car) {
		int constructorComparison = this.constructor.compareTo(car.constructor);
		int modelComparison = this.model.compareTo(car.model);

		if (this.constructor.compareTo(car.constructor) != 0) {
			return constructorComparison;
		} else {
			return modelComparison;	
		}
	}
	
	public String toString() {
		return "(" + constructor + ", " + model + ", " + quantity + ", " + leftTotal + ")";
	}
}
