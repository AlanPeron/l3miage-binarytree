package main.final20222023;

import impl.BinaryTree;
import impl.Iterator;
import interfaces.Node;

public class Garage extends BinaryTree<Car> {
	public Garage() throws Exception {
		super();
		setUp();
	}

	public void setUp() throws Exception {
		this.rootNode = new Node<Car>(new Car("c1", "m2", 20, 1));
		
		Iterator<Car> iterator = this.iterator();
		iterator.goLeft();
		iterator.addValue(new Car("c1", "m1", 10, 0));
		iterator.goUp();
		iterator.goRight();
		iterator.addValue(new Car("c2", "m6", 5, 3));
		iterator.goLeft();
		iterator.addValue(new Car("c2", "m5", 22, 2));
		iterator.goLeft();
		iterator.addValue(new Car("c1", "m3", 7, 0));
		iterator.goRight();
		iterator.addValue(new Car("c2", "m4", 11, 0));
		iterator.goUp();
		iterator.goUp();
		iterator.goUp();
		iterator.goRight();
		iterator.addValue(new Car("c3", "m91", 2, 3));
		iterator.goLeft();
		iterator.addValue(new Car("c3", "m8", 10, 1));
		iterator.goLeft();
		iterator.addValue(new Car("c3", "m7", 6, 0));
		iterator.goUp();
		iterator.goRight();
		iterator.addValue(new Car("c3", "m9", 10, 0));
	}

	/**
	 * To Implement
	 * @throws Exception 
	 */
	public int getQuantity(Car car) throws Exception {
		return 0;
	}

	/**
	 * To Implement
	 */
	public int countCars(String constructor) {
		return 0;
	}

	/**
	 * To Implement
	 */
	public int total() {
		return 0;
	}

	/**
	 * To Implement
	 */
	public Car rightmostLeaf() {
		return new Car("", "", 0, 0);
	}

	/**
	 * To Implement
	 */
	public void printClosestLeaf() {

	}
}
